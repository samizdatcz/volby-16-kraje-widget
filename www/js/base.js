(function(){
  var body;
  document.body.removeChild(document.getElementById('fallback'));

  window.ig.data2 = {};

  var CRaggr = {};
  CRaggr.kraj = "celou ČR";
  CRaggr.titul = 0.437415881561238;
  CRaggr.novacci = 0.761187752355316;
  CRaggr.vek = 46.8435397039031;
  CRaggr.zeny = 0.295255720053836;
  CRaggr.max = 15490;
  CRaggr.pocet = 11888;
  CRaggr.special = 1;
  window.ig.data2.cr = CRaggr;


  var defaultKraj = "Jihomoravský kraj";


  var remaining = 3;


  var createHistogram = function(data) {
    // 18-25, 26-35, 36-45, ... 75+

    var ages = data.split(" ");
    var total = ages.length;
    var histogramLimits = [0, 25, 35, 45, 55, 65, 75].reverse();
    var histogram = [0, 0, 0, 0, 0, 0, 0];
    for(var j = 0; j < ages.length; j++) {
      var a = parseInt(ages[j]);
      for(var k = 0; k < histogramLimits.length; k++) {
        if(a > histogramLimits[k]) {
          histogram[k] = histogram[k] + 1;
          break;
        }
      };
      
    }

    return histogram.map(function(val) {
      return Math.round((val/total)*100);
    }).reverse();

    
    

  }


  d3.csv("../data/vizitky.csv", function(err, strany){

      window.ig.data2.histograms = {};
      for(var i = 0; i < strany.length; i++) {
        var strana = strany[i];
        window.ig.data2.histograms[strana.histovek] = createHistogram(strana.histovek);
      }

      window.ig.data2.strany = strany;
      if (!--remaining) window.ig.createUi();
  });

  d3.csv("../data/histovek.csv", function(err, kraje){

      window.ig.data2.histogramsKraje = {};
      for(var i = 0; i < kraje.length; i++) {
        var kraj = kraje[i];
        var tmp = [
          kraj["pod25"],
          kraj["26az35"],
          kraj["36az45"],
          kraj["46az55"],
          kraj["56az65"],
          kraj["66az75"],
          kraj["nad75"],
          ];

         window.ig.data2.histogramsKraje[kraj.kraj] = tmp.map(function(item) {
          return Math.round(parseFloat(item.replace(/,/, ".")));
         }); 
      }

      
      if (!--remaining) window.ig.createUi();
  });

  d3.csv("../data/vizitkykraje.csv", function(err, kraje){
      
      window.ig.data2.kraje = {};
      kraje.forEach(function(kraj) {
        window.ig.data2.kraje[kraj.kraj] = kraj;
      });

      if (!--remaining) window.ig.createUi();
  });

  window.ig.createUi = function(strany) {
    
    var container, x$, wrap, y$, content, heading, subheading, kostiColors, z$, tableContainer, z1$, closeBtn, tableHeadings, hasTitul, groupVek;
    var container = d3.select('.ig');
    var s = container.append("strong");
    s.text("Zvolte kraj: ");
    var selectBox = container.append("select");
    selectBox.attr("class", "kandidatka-select");
    window.ig.createKrajOptions(selectBox);

    selectBox.on("change", function() {
      window.ig.showKandidatka(tableContainer, window.ig.filterStrany(this.value));
    });

    selectBox.property("value", defaultKraj);

    x$ = wrap = container.append('div');
    x$.attr('class', 'kandidatka-wrap');
    y$ = content = wrap.append('div');
    y$.attr('class', 'kandidatka');
    z$ = tableContainer = content.append('div');
    z$.attr('class', 'tableContainer');

    window.ig.showKandidatka(tableContainer, window.ig.filterStrany(defaultKraj));
  

  };

  window.ig.filterStrany = function(kraj) {
    return window.ig.data2.strany.filter(function(item) {
      return item.kraj == kraj;
    }).concat(window.ig.data2.kraje[kraj]).concat(window.ig.data2.cr);
  };

  window.ig.createKrajOptions = function(selectBox) {
    var kraje = {};
    window.ig.data2.strany.forEach(function(val) {
      kraje[val.kraj] = val.kraj;
    });
    

    for (var kraj in kraje) {
      if (kraje.hasOwnProperty(kraj)) {
        var option = selectBox.append("option");
        option.attr("val", kraje[kraj]);
        option.text(kraje[kraj]);
      }
    }
  }

  new Tooltip().watchElements();

  
}).call(this);
